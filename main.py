from fastapi import FastAPI
from fastapi import HTTPException
from typing import List
from models import Pin, User
import sqlite3

app = FastAPI()

db_name = "database.db"
# Create a connection to the SQLite database
conn = sqlite3.connect(db_name)

# Initialize the cursor
c = conn.cursor()

# Create a User table in the database
c.execute('''CREATE TABLE IF NOT EXISTS users
             (id INTEGER PRIMARY KEY AUTOINCREMENT,
              username TEXT UNIQUE,
              password TEXT)''')

# Create a Pin table in the database
c.execute('''CREATE TABLE IF NOT EXISTS pins
             (id INTEGER PRIMARY KEY AUTOINCREMENT,
              title TEXT,
              content TEXT,
              source TEXT,
              created_at TEXT,
              user_id INTEGER,
              FOREIGN KEY (user_id) REFERENCES users(id))''')

conn.close()


# Endpoint to create a new Pin
@app.post("/pin")
async def create_pin(pin: Pin):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    try:
        # Insert the new Pin into the database
        c.execute('''INSERT INTO pins
                     (title, content, source, created_at, user_id)
                     VALUES (?, ?, ?, ?, ?)''', (pin.title, pin.content, pin.source, pin.created_at, pin.user_id))
        pin_id = c.lastrowid
        conn.commit()
        conn.close()
        # Return the new Pin
        return {**pin.dict(), "id": pin_id}
    except:
        conn.close()
        raise HTTPException(status_code=400, detail="Failed to create Pin")

# Endpoint to read all Pins
@app.get("/pin")
async def read_all_pins():
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Retrieve all Pins from the database
    c.execute('''SELECT id, title, content, source, created_at, user_id FROM pins''')
    pins = []
    for row in c.fetchall():
        pins.append({"id": row[0], "title": row[1], "content": row[2], "source": row[3], "created_at": row[4], "user_id": row[5]})
    conn.close()
    # Return all Pins
    return pins

# Endpoint to read a specific Pin
@app.get("/pin/{pin_id}")
async def read_pin(pin_id: int):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Retrieve the specified Pin from the database
    c.execute('''SELECT id, title, content, source, created_at, user_id FROM pins WHERE id = ?''', (pin_id,))
    row = c.fetchone()
    conn.close()
    # If the Pin exists, return it. Otherwise, raise an HTTPException.
    if row:
        return {"id": row[0], "title": row[1], "content": row[2], "source": row[3], "created_at": row[4], "user_id": row[5]}
    else:
        raise HTTPException(status_code=404, detail="Pin not found")

# Endpoint to update a specific Pin
@app.put("/pin/{pin_id}")
async def update_pin(pin_id: int, pin: Pin):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Check if the specified Pin exists in the database
    c.execute('''SELECT id FROM pins WHERE id = ?''', (pin_id,))
    row = c.fetchone()
    if row:
        # Update the specified Pin in the database
        c.execute('''UPDATE pins SET title = ?, content = ?, source = ?, created_at = ?, user_id = ?
                     WHERE id = ?''', (pin.title, pin.content, pin.source, pin.created_at, pin.user_id, pin_id))
        conn.commit()
        conn.close()
        # Return the updated Pin
        return {**pin.dict(), "id": pin_id}
    else:
        conn.close()
        # Raise an HTTPException if the Pin does not exist
        raise HTTPException(status_code=404, detail="Pin not found")

# Endpoint to delete a specific Pin
@app.delete("/pin/{pin_id}")
async def delete_pin(pin_id: int):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Check if the specified Pin exists in the database
    c.execute('''SELECT id FROM pins WHERE id = ?''', (pin_id,))
    row = c.fetchone()
    if row:
        # Delete the specified Pin from the database
        c.execute('''DELETE FROM pins WHERE id = ?''', (pin_id,))
        conn.commit()
        conn.close()
        # Return a message confirming deletion
        return {"message": "Pin deleted successfully"}
    else:
        conn.close()
        # Raise an HTTPException if the Pin does not exist
        raise HTTPException(status_code=404, detail="Pin not found")

    conn.close()
    # Raise an HTTPException if the username already exists
    raise HTTPException(status_code=400, detail="Username already taken")

# Endpoint to create a new User
@app.post("/user")
async def create_user(user: User):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Check if the specified username already exists in the database
    c.execute('''SELECT id FROM users WHERE username = ?''', (user.username,))
    row = c.fetchone()
    if row:
        conn.close()
        # Raise an HTTPException if the username already exists
        raise HTTPException(status_code=400, detail="Username already taken")
    else:
        # Insert the new User into the database
        c.execute('''INSERT INTO users (username, password) VALUES (?, ?)''', (user.username, user.password))
        conn.commit()
        # Get the ID of the new User from the database
        user_id = c.lastrowid
        conn.close()
        # Return the new User with its ID
        return {**user.dict(), "id": user_id}

# Endpoint to get all Users
@app.get("/user")
async def read_users():
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Select all Users from the database
    c.execute('''SELECT id, username FROM users''')
    rows = c.fetchall()
    conn.close()
    # Convert the list of tuples to a list of dictionaries
    users = [{"id": row[0], "username": row[1]} for row in rows]
    # Return the list of Users
    return users


# Endpoint to get a specific User by ID
@app.get("/user/{user_id}")
async def read_user(user_id: int):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Check if the specified User exists in the database
    c.execute('''SELECT id, username FROM users WHERE id = ?''', (user_id,))
    row = c.fetchone()
    if row:
        conn.close()
        # Convert the tuple to a dictionary and return it
        return {"id": row[0], "username": row[1]}
    else:
        conn.close()
        # Raise an HTTPException if the User does not exist
        raise HTTPException(status_code=404, detail="User not found")


# Endpoint to update a specific User by ID
@app.put("/user/{user_id}")
async def update_user(user_id: int, user: User):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Check if the specified User exists in the database
    c.execute('''SELECT id FROM users WHERE id = ?''', (user_id,))
    row = c.fetchone()
    if row:
        # Update the specified User in the database
        c.execute('''UPDATE users SET username = ?, password = ? WHERE id = ?''', (user.username, user.password, user_id))
        conn.commit()
        conn.close()
        # Return the updated User
        return {**user.dict(), "id": user_id}
    else:
        conn.close()
        # Raise an HTTPException if the User does not exist
        raise HTTPException(status_code=404, detail="User not found")

# Endpoint to delete a specific User by ID
@app.delete("/user/{user_id}")
async def delete_user(user_id: int):
    conn = sqlite3.connect(db_name)
    c = conn.cursor()
    # Check if the specified User exists in the database
    c.execute('''SELECT id FROM users WHERE id = ?''', (user_id,))
    row = c.fetchone()
    if row:
        # Delete the specified User from the database
        c.execute('''DELETE FROM users WHERE id = ?''', (user_id,))
        conn.commit()
        conn.close()
        # Return a message indicating that the User has been deleted
        return {"message": "User deleted successfully"}
    else:
        conn.close()
        # Raise an HTTPException if the User does not exist
        raise HTTPException(status_code=404, detail="User not found")