from pydantic import BaseModel


class Pin(BaseModel):
    title: str
    content: str
    source: str
    created_at: str
    user_id: int


class User(BaseModel):
    username: str
    password: str
